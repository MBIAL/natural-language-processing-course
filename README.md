# Natural Language Processing Course



## Introduction

This course teaches an overview of modern Natural Language Processing methods. In particular, text processing techniques and language models. This course mainly fit to NLP begineer and professional.

## Program
 Introduction

 Part I: Text pre processing
  * ChapterI: Text basic threatment
  * Chapter II: Advanced text threatment

 Part II: Language models
  * Chapter I: tokens and embeddings
  * Chapter II: basic neurals networks
  * Chapter III: RNN and LSTM models
  * Chapter IV: Attention models
  * Chapter V: Transformers
  * Chapter VI: Graph neurals networks

## Exercises:
The weekly exercises consist of a mix of theoretical and practical Python exercises for the corresponding topic (starting part 2).


## Authors and acknowledgment
MBIA NDI Marie Thérèse

